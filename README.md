# HelloWorld

This is readme.md in master branch fix in visual code.
This is readme.md in master branch fix on webapp.
This is in feature1 branch.

## Git flow  

Làm việc với `develop` branch thay vì master.

### Feature  

Fork từ `develop` branch . Thay vì dùng 2 lệnh :  

```bash
git checkout develop
git checkout -b feature_branch
```

Ta chỉ cần 1 lệnh :  

```bash
git flow feature start feature_branch
```

Khi kết thúc :  

```bash
git flow feature finish feature_branch
```

### Release  

Ta nên tạo một `release` branch để có thể review những thay đổi. Sau khi xác nhận thay đổi, branch này sẽ được merge với `master` và `develop`.  

```bash
git flow release start 0.1.0
git flow release finish '0.1.0'
```

### Hotfix  

Hotfix là branch duy nhất nên được fork từ master. Sau khi hoàn thành thì nên được merge vào cả master và develop.  

```bash
git flow hotfix start hotfix_branch
git flow hotfix finish hotfix_branch
```

